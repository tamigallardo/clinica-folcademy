package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/paciente")
public class PacienteController {

    private final PacienteService pacienteService;

    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }


    //@PreAuthorize("hasAuthority('get_pacientes')")
    @GetMapping(value = "")
    public ResponseEntity<List<PacienteDto>> listarTodo() {
        return ResponseEntity.ok(pacienteService.listarTodos());
    }

    //@PreAuthorize("hasAuthority('get_pacientes')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<List<Paciente>> findAll(@PathVariable(name = "id") Integer id) {
        return ResponseEntity
                .ok()
                .body(
                        pacienteService.findPacienteById(id))
                ;
    }

    //@PreAuthorize("hasAuthority('post_pacientes')")
    @PostMapping("")
    public ResponseEntity <Paciente> agregar(@RequestBody @Validated Paciente entity){
        return ResponseEntity.ok(pacienteService.agregar(entity));
    }

    //@PreAuthorize("hasAuthority('put_pacientes')")
    @PutMapping("/{idPaciente}")
    public ResponseEntity<Paciente> editar(@PathVariable (name="idPaciente") int id,
                                           @RequestBody Paciente entity){
        return ResponseEntity.ok(pacienteService.editar(id,entity));
    }

    //@PreAuthorize("hasAuthority('delete_pacientes')")
    @DeleteMapping("{idPaciente}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name="idPaciente") int id){
        return ResponseEntity.ok(pacienteService.eliminar(id));
    }
}
