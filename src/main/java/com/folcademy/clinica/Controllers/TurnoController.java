package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.*;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/turno")
public class TurnoController {

    private final TurnoService turnoService;

    public TurnoController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }

    //@PreAuthorize("hasAuthority('post_turnos')")
    @PostMapping("")
    public ResponseEntity<TurnoDto> create(@RequestBody @Validated TurnoDto entity)
    {
        return ResponseEntity.ok(turnoService.create(entity));
    }

    //@PreAuthorize("hasAuthority('get_turnos')")
    @GetMapping("")
    public ResponseEntity<List<TurnoMostrarDto>> listarTodos(){

        return ResponseEntity.ok(turnoService.listarTodos());
    }

    //@PreAuthorize("hasAuthority('get_turnos')")
    @GetMapping(value = "/{idTurno}")
    public ResponseEntity <List<TurnoMostrarDto>> findAll(@PathVariable(name = "idTurno") int id){

        return ResponseEntity.ok(turnoService.findTurnoById(id));
    }

    //@PreAuthorize("hasAuthority('put_turnos')")
    @PutMapping("/{idTurno}")
    public ResponseEntity<TurnoEnteroDto> editar(@PathVariable(name="idTurno") int id,
                                                           @RequestBody TurnoEnteroDto dto){
        return ResponseEntity.ok(turnoService.editar(id,dto));
    }

    //@PreAuthorize("hasAuthority('delete_turnos')")
    @DeleteMapping("/{idTurno}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name="idTurno") int id){
        return ResponseEntity.ok(turnoService.eliminar(id));
    }
}
