package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Exceptions.ValidationException;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class PacienteService {
    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;
    public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper) {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
    }

    public List<PacienteDto> listarTodos() {
        return pacienteRepository.findAll().stream().map(pacienteMapper::entityToDto).collect(Collectors.toList());
    }

    public List<Paciente> findPacienteById(Integer id) {
        if(!pacienteRepository.existsById(id))
            throw new NotFoundException("No existe paciente con ese id");
        List<Paciente> lista = new ArrayList<>();
        Paciente paciente = pacienteRepository.findById(id).get();
        lista.add(paciente);
        return lista;
    }

    public Paciente agregar(Paciente entity){
        Pattern pattern = Pattern.compile("^[a-zA-Z]+$");
        Pattern numeros = Pattern.compile("^[0-9]+$");
        Matcher nombre = pattern.matcher(entity.getNombre());
        Matcher apellido = pattern.matcher(entity.getApellido());
        Matcher dni = numeros.matcher(entity.getDni());
        entity.setIdpaciente(null);
        if(!dni.find())
           throw new ValidationException("El DNI no debe contener letras");
        if(!nombre.find() || !apellido.find()) {
            throw new ValidationException("El nombre y apellido no deben contener números");
        }
        return pacienteRepository.save(entity);
    }

    public Paciente editar(Integer idPaciente, Paciente entity){
        if(!pacienteRepository.existsById(idPaciente))
            throw new NotFoundException("No existe paciente con ese id");
        if(entity.getDni()==null)
            throw new BadRequestException("Se debe ingresar el DNI");
        entity.setIdpaciente(idPaciente);
        return pacienteRepository.save(entity);
    }

    public Boolean eliminar(Integer id){
        if(!pacienteRepository.existsById(id))
            throw new NotFoundException("No existe paciente con ese id");
        pacienteRepository.deleteById(id);
        return true;
    }
}
