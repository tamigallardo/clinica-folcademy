package com.folcademy.clinica.Services;

import java.util.regex.*;
import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Exceptions.ValidationException;
import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("medicoService")
public class MedicoService {
    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;
    public MedicoService(MedicoRepository medicoRepository, MedicoMapper medicoMapper) {
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
    }

    public List<MedicoDto> listarTodos(){

        return medicoRepository.findAll().stream().map(medicoMapper::entityToDto).collect(Collectors.toList());
    }
    public MedicoDto listarUno(Integer id){
        if(!medicoRepository.existsById(id))
            throw new NotFoundException("No existe médico con ese id");
        return medicoRepository.findById(id).map(medicoMapper::entityToDto).orElse(null);  //si viene un medico vacio es pq no enontro pero no se rompio el program
    }

    public MedicoDto agregar(MedicoDto entity){
        entity.setId(null);
        if(entity.getConsulta()<0)
            throw new BadRequestException("La consulta no puede ser menor a 0");
        if(entity.getProfesion() == null)
            throw new BadRequestException("Se debe ingresar una profesion");
        return medicoMapper.entityToDto(medicoRepository.save(medicoMapper.dtoToEntity(entity)));
    }

    public MedicoEnteroDto editar(Integer idMedico, MedicoEnteroDto dto){
        Pattern pattern = Pattern.compile("^[a-zA-Z]+$");
        Matcher matcher = pattern.matcher(dto.getNombre());
        if(!medicoRepository.existsById(idMedico))
            throw new NotFoundException("No existe médico con ese id");
        dto.setId(idMedico);
        if(!matcher.find())
            throw new ValidationException("El nombre no debe contener números");
        return medicoMapper.entityToEnteroDto(
                medicoRepository.save(
                        medicoMapper.enteroDtoToEntity(
                                dto
                        )
                )
        );
    }

    public Boolean editarConsulta(Integer idMedico, Integer consulta){
        if(medicoRepository.existsById(idMedico)){
            Medico entity = medicoRepository.findById(idMedico).orElse(new Medico());
            if(consulta<0)
                throw new BadRequestException("La consulta no puede ser menor a 0");
            entity.setConsulta(consulta);
            medicoRepository.save(entity);
            return true;
        }
        throw new NotFoundException("No existe médico con ese id");
    }

    public Boolean eliminar(Integer id){
        if(!medicoRepository.existsById(id))
            throw new NotFoundException("No existe médico con ese id");
        medicoRepository.deleteById(id);
        return true;
    }
}
