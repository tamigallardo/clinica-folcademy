package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.*;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TurnoService {
    private final TurnoMapper turnoMapper;
    private final TurnoRepository turnoRepository;
    private final MedicoRepository medicoRepository;
    private final PacienteRepository pacienteRepository;
    public TurnoService(TurnoMapper turnoMapper, TurnoRepository turnoRepository, MedicoRepository medicoRepository, PacienteRepository pacienteRepository) {
        this.turnoMapper = turnoMapper;
        this.turnoRepository = turnoRepository;
        this.medicoRepository = medicoRepository;
        this.pacienteRepository = pacienteRepository;
    }

    public TurnoDto create(TurnoDto entity){
        entity.setId(null);
        int idmedico = entity.getIdmedico().getId();
        int idpaciente = entity.getIdpaciente().getIdpaciente();
        if(medicoRepository.existsById(idmedico) && pacienteRepository.existsById(idpaciente))
        {
            Time hora = entity.getHora();
            Time abre= new Time(8,00,00),
                cierra = new Time(19,00,00);
            if(hora.getTime() <= cierra.getTime() && hora.getTime() >= abre.getTime())
                return turnoMapper.entityToDto(turnoRepository.save(turnoMapper.dtoToEntity(entity,entity.getIdmedico(),entity.getIdpaciente())));
            throw new BadRequestException("Ingrese una hora entre las 08:00:00 y 19:00:00");
        }
        throw new BadRequestException("No existe médico o paciente con el ID ingresado!");
    }

    public List<TurnoMostrarDto> listarTodos(){

        return turnoRepository.findAll().stream().map(turnoMapper::mostrarEntityToDto).collect(Collectors.toList());
    }

    public List<TurnoMostrarDto> findTurnoById(Integer id){
        if(!turnoRepository.existsById(id))
            throw new NotFoundException("No existe turno con el ID ingresado");
        List<TurnoMostrarDto> lista = new ArrayList<>();
        TurnoMostrarDto turno= turnoRepository.findById(id).map(turnoMapper::mostrarEntityToDto).orElse(null);
        lista.add(turno);
        return lista;
    }


    public TurnoEnteroDto editar (Integer idTurno,TurnoEnteroDto dto){
        if(!turnoRepository.existsById(idTurno))
            throw new NotFoundException("No existe turno con el ID ingresado");
        Time hora = dto.getHora();
        Time abre= new Time(8,00,00),
                cierra = new Time(19,00,00);
        if(hora.getTime() <= cierra.getTime() && hora.getTime() >= abre.getTime()) {
            dto.setId(idTurno);
            return turnoMapper.entityToEnteroDto(
                turnoRepository.save(
                        turnoMapper.enteroDtoToEntity(dto)
                )
            );
        }
        throw new BadRequestException("Ingrese una hora entre las 08:00:00 y 19:00:00");
    }

    public Boolean eliminar(Integer id){
        if(!turnoRepository.existsById(id))
            throw new NotFoundException("No existe turno con el ID ingresado");
        turnoRepository.deleteById(id);
        return true;
    }
}
