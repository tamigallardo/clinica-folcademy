package com.folcademy.clinica.Model.Entities;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.sql.Time;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "turno")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Turno {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idturno", columnDefinition = "INT(10) UNSIGNED")
    public Integer id;

    @Column(name = "fecha")
    public Date fecha;

    @Column(name = "hora", nullable = false)
    public Time hora;

    @Column(name = "atendido",columnDefinition = "TINYINT(0)")
    private Boolean atendido;

    @ManyToOne(optional = false,cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
    @JoinColumn(name = "idpaciente")
    private Paciente idpaciente;

    @ManyToOne(optional = false,cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
    @JoinColumn(name = "idmedico")
    private Medico idmedico;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Turno turno = (Turno) o;
        return id != null && Objects.equals(id, turno.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
