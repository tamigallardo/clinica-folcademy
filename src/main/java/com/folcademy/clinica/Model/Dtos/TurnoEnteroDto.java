package com.folcademy.clinica.Model.Dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TurnoEnteroDto {

    Integer id;
    Date fecha;
    Time hora;
    Boolean atendido;
    PacienteDto idpaciente;
    MedicoDto idmedico;
}
