package com.folcademy.clinica.Model.Dtos;

import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class TurnoMostrarDto {
    Integer id;

    @NotNull
    Date fecha;

    @NotNull
    Time hora;

    PacienteDto idpaciente;
    MedicoDto idmedico;
}
