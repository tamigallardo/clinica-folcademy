package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.*;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Turno;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class TurnoMapper {
    private final PacienteMapper pacienteMapper;
    private final MedicoMapper medicoMapper;

    public TurnoMapper(PacienteMapper pacienteMapper, MedicoMapper medicoMapper) {
        this.pacienteMapper = pacienteMapper;
        this.medicoMapper = medicoMapper;
    }

    public TurnoDto entityToDto(Turno entity){
        return Optional
                .ofNullable(entity)
                .map(
                        enti -> new TurnoDto(
                                enti.getId(),
                                enti.getFecha(),
                                enti.getHora(),
                                enti.getIdpaciente(),
                                enti.getIdmedico()
                                )
                        )
                .orElse(new TurnoDto());
    }

    public Turno dtoToEntity (TurnoDto dto, Medico medico, Paciente paciente){
        Turno entity = new Turno();
        entity.setId(dto.getId());
        entity.setFecha(dto.getFecha());
        entity.setHora(dto.getHora());
        entity.setIdpaciente(paciente);
        entity.setIdmedico(medico);
        return entity;
    }

    public TurnoEnteroDto entityToEnteroDto(Turno entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoEnteroDto(
                                ent.getId(),
                                ent.getFecha(),
                                ent.getHora(),
                                ent.getAtendido(),
                                pacienteMapper.entityToDto(ent.getIdpaciente()),
                                medicoMapper.entityToDto(ent.getIdmedico())
                        )
                )
                .orElse(new TurnoEnteroDto());
    }

    public Turno enteroDtoToEntity (TurnoEnteroDto dto){
        Turno entity = new Turno();
        entity.setId(dto.getId());
        entity.setFecha(dto.getFecha());
        entity.setHora(dto.getHora());
        entity.setAtendido(dto.getAtendido());
        entity.setIdpaciente(pacienteMapper.dtoToEntity(dto.getIdpaciente()));
        entity.setIdmedico(medicoMapper.dtoToEntity(dto.getIdmedico()));
        return entity;
    }

    public TurnoMostrarDto mostrarEntityToDto(Turno entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoMostrarDto(
                                ent.getId(),
                                ent.getFecha(),
                                ent.getHora(),
                                pacienteMapper.entityToDto(ent.getIdpaciente()),
                                medicoMapper.entityToDto(ent.getIdmedico())
                        )
                )
                .orElse(new TurnoMostrarDto());
    }

    public Turno mostarDtoToEntity (TurnoMostrarDto dto){
        Turno entity = new Turno();
        entity.setId(dto.getId());
        entity.setFecha(dto.getFecha());
        entity.setHora(dto.getHora());
        entity.setIdpaciente(pacienteMapper.dtoToEntity(dto.getIdpaciente()));
        entity.setIdmedico(medicoMapper.dtoToEntity(dto.getIdmedico()));
        return entity;
    }
}
